package com.example.demo.controller;

import com.example.demo.model.request.InfoRequest;
import com.example.demo.model.request.RegisterRequest;
import com.example.demo.model.response.InfoResponse;
import com.example.demo.model.response.RegisterResponse;
import com.example.demo.service.GetInfoService;
import com.example.demo.service.RegisterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/v1")
public class CustomerInformationController {

    private final RegisterService registerService;
    private final GetInfoService getInfoService;

    public CustomerInformationController(RegisterService registerService,
            GetInfoService getInfoService) {
        this.registerService = registerService;
        this.getInfoService = getInfoService;
    }

    @PostMapping(value = "/register")
    public RegisterResponse getNonLifePlan(@RequestBody RegisterRequest request) throws Exception {
        return registerService.execute(request);
    }

    @GetMapping(value = "/info/{username}")
    public InfoResponse getInfoWithId(@RequestHeader("Authorization") String jwt,@PathVariable String username)
            throws Exception {
        InfoRequest request = InfoRequest.builder()
                .jwt(jwt)
                .userName(username)
                .build();
        return getInfoService.execute(request);
    }

    @GetMapping(value = "/info/")
    public InfoResponse getInfo(@RequestHeader("Authorization") String jwt) throws Exception {
        InfoRequest request = InfoRequest.builder()
                .jwt(jwt)
                .build();
        return getInfoService.execute(request);
    }
}

package com.example.demo.repository;

import com.example.demo.model.entity.RegisterInfoEntity;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterInfoRepository extends CrudRepository<RegisterInfoEntity,String> {

    Optional<RegisterInfoEntity> findByUserNameAndIsActiveTrue(String userName);
}

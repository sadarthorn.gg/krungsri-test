package com.example.demo.model.request;

import com.example.demo.helper.model.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterRequest extends BaseRequest {

    private String userName;
    private String password;
    private String firstName;
    private String middleName;
    private String lastName;
    private int salary;
    private String address1;
    private String address2;
    private String phoneNo;
    private String email;
}

package com.example.demo.model.response;

import com.example.demo.helper.model.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class InfoResponse extends BaseResponse {

    private String userName;
    private String password;
    private String firstName;
    private String middleName;
    private String lastName;
    private int salary;
    private String memberType;
    private String address1;
    private String address2;
    private String phoneNo;
    private String email;
    private String referCode;
    private boolean isActive;
}

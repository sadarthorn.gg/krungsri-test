package com.example.demo.helper.util;

import com.example.demo.model.entity.JwtEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.springframework.stereotype.Component;

@Component
public class JwtHelper {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public JwtEntity parseBody(String token) throws IOException {
        String body = new String(Base64.getUrlDecoder().decode(token.split("\\.")[1]), StandardCharsets.UTF_8);
        return objectMapper.readValue(body, JwtEntity.class);
    }
}

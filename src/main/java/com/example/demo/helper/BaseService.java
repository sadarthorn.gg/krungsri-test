package com.example.demo.helper;

import com.example.demo.helper.model.BaseRequest;
import com.example.demo.helper.model.BaseResponse;

public interface BaseService <T extends BaseRequest, V extends BaseResponse> {
    V execute(T var1) throws Exception;
}

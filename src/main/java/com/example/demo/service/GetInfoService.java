package com.example.demo.service;

import com.example.demo.helper.BaseService;
import com.example.demo.helper.util.JwtHelper;
import com.example.demo.model.entity.JwtEntity;
import com.example.demo.model.entity.RegisterInfoEntity;
import com.example.demo.model.request.InfoRequest;
import com.example.demo.model.response.InfoResponse;
import com.example.demo.repository.RegisterInfoRepository;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class GetInfoService implements
        BaseService<InfoRequest, InfoResponse> {

    private final RegisterInfoRepository registerInfoRepository;
    private final JwtHelper jwtHelper;

    public GetInfoService(RegisterInfoRepository registerInfoRepository,
            JwtHelper jwtHelper) {
        this.registerInfoRepository = registerInfoRepository;
        this.jwtHelper = jwtHelper;
    }

    @Override
    public InfoResponse execute(InfoRequest request) throws Exception {
        JwtEntity jwt = jwtHelper.parseBody(request.getJwt());
        RegisterInfoEntity entity;
        if (Objects.isNull(request.getUserName())) {
            entity = registerInfoRepository.findByUserNameAndIsActiveTrue(jwt.getUsername()).get();
        } else {
            String admin = "admin";
            if (admin.equals(jwt.getRole())) {
                entity = registerInfoRepository.findByUserNameAndIsActiveTrue(request.getUserName()).get();
            } else {
                throw new Exception("This user don't have permission to get information customer");
            }

        }
        return InfoResponse.builder()
                .userName(entity.getUserName())
                .password(entity.getPassword())
                .firstName(entity.getFirstName())
                .middleName(entity.getMiddleName())
                .lastName(entity.getLastName())
                .salary(entity.getSalary())
                .memberType(entity.getMemberType())
                .address1(entity.getAddress1())
                .address2(entity.getAddress2())
                .phoneNo(entity.getPhoneNo())
                .email(entity.getEmail())
                .referCode(entity.getReferCode())
                .isActive(entity.isActive())
                .build();
    }
}

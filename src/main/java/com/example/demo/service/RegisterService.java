package com.example.demo.service;

import com.example.demo.helper.BaseService;
import com.example.demo.model.entity.RegisterInfoEntity;
import com.example.demo.model.request.RegisterRequest;
import com.example.demo.model.response.RegisterResponse;
import com.example.demo.repository.RegisterInfoRepository;
import java.text.SimpleDateFormat;
import java.time.temporal.ValueRange;
import java.util.Calendar;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RegisterService implements
        BaseService<RegisterRequest, RegisterResponse> {

    private final RegisterInfoRepository registerInfoRepository;

    public RegisterService(RegisterInfoRepository registerInfoRepository) {
        this.registerInfoRepository = registerInfoRepository;
    }


    @Override
    public RegisterResponse execute(RegisterRequest request) throws Exception {
        if (isUserNameCanUse(request.getUserName())) {
            throw new Exception("80000 This User cannot be used");
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
        var referCode = timeStamp.concat(request.getPhoneNo().substring(6, 10));
        var memberType = getMemberType(request.getSalary());
        RegisterInfoEntity entity = RegisterInfoEntity.builder()
                .userName(request.getUserName())
                .password(request.getPassword())
                .firstName(request.getFirstName())
                .middleName(request.getMiddleName())
                .lastName(request.getLastName())
                .salary(request.getSalary())
                .memberType(memberType)
                .address1(request.getAddress1())
                .address2(request.getAddress2())
                .phoneNo(request.getPhoneNo())
                .email(request.getEmail())
                .referCode(referCode)
                .isActive(true)
                .build();
        registerInfoRepository.save(entity);
        return RegisterResponse.builder()
                .referenceCode(referCode)
                .memberType(memberType)
                .build();
    }

    private String getMemberType(int salary) throws Exception {
        if (salary >= 50000) {
            return "Platinum";
        } else if (ValueRange.of(30000, 49999).isValidValue(salary)) {
            return "Gold";
        } else if (ValueRange.of(15000, 29999).isValidValue(salary)) {
            return "Silver";
        } else {
            throw new Exception("80001");
        }
    }

    private boolean isUserNameCanUse(String userName) {
        return registerInfoRepository.findByUserNameAndIsActiveTrue(userName).isPresent();
    }
}

CREATE TABLE tbl_register_info (
username varchar(200) NOT NULL,
password varchar(200) NOT NULL,
first_name varchar(200) NOT NULL,
middle_name varchar(200),
last_name varchar(200) NOT NULL,
salary int NOT NULL,
member_type varchar(200) NOT NULL,
address1 varchar(200) NOT NULL,
address2 varchar(200),
phone_number varchar(200) NOT NULL,
email varchar(200) NOT NULL,
refer_code varchar(200) NOT NULL,
is_active boolean NOT NULL,
PRIMARY KEY (username)
);

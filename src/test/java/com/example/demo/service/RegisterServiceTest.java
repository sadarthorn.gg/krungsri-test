package com.example.demo.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.example.demo.model.entity.RegisterInfoEntity;
import com.example.demo.model.request.RegisterRequest;
import com.example.demo.model.response.RegisterResponse;
import com.example.demo.repository.RegisterInfoRepository;
import java.util.Optional;
import lombok.var;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RegisterServiceTest {

    @InjectMocks
    private RegisterService registerService;

    @Mock
    private RegisterInfoRepository registerInfoRepository;

    @Before
    public void setUp() {

    }

    @Test
    public void whenRegisterPlatinum() throws Exception {
        var request = RegisterRequest.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(52000)
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .build();
        when(registerInfoRepository.findByUserNameAndIsActiveTrue(any())).thenReturn(Optional.empty());
        RegisterResponse actual = registerService.execute(request);
        Assert.assertNotNull(actual);
    }

    @Test
    public void whenRegisterGold() throws Exception {
        var request = RegisterRequest.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(40000)
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .build();
        when(registerInfoRepository.findByUserNameAndIsActiveTrue(any())).thenReturn(Optional.empty());
        RegisterResponse actual = registerService.execute(request);
        Assert.assertNotNull(actual);
    }

    @Test
    public void whenRegisterSilver() throws Exception {
        var request = RegisterRequest.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(16000)
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .build();
        when(registerInfoRepository.findByUserNameAndIsActiveTrue(any())).thenReturn(Optional.empty());
        RegisterResponse actual = registerService.execute(request);
        Assert.assertNotNull(actual);
    }

    @Test
    public void whenRegisterReject() throws Exception {
        var request = RegisterRequest.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(14000)
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .build();
        when(registerInfoRepository.findByUserNameAndIsActiveTrue(any())).thenReturn(Optional.empty());
        var exception = Assert.assertThrows(Exception.class, () -> registerService.execute(request));
        assertEquals("80001", exception.getMessage());
    }

    @Test
    public void whenRegisterUserDuplicate() throws Exception {
        var request = RegisterRequest.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(16000)
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .build();
        when(registerInfoRepository.findByUserNameAndIsActiveTrue(any())).thenReturn(Optional.of(RegisterInfoEntity.builder()
                .build()));
        var exception = Assert.assertThrows(Exception.class, () -> registerService.execute(request));
        assertEquals("80000 This User cannot be used", exception.getMessage());
    }
}
package com.example.demo.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.example.demo.helper.util.JwtHelper;
import com.example.demo.model.entity.JwtEntity;
import com.example.demo.model.entity.RegisterInfoEntity;
import com.example.demo.model.request.InfoRequest;
import com.example.demo.model.response.InfoResponse;
import com.example.demo.repository.RegisterInfoRepository;
import java.util.Optional;
import lombok.var;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GetInfoServiceTest {

    @InjectMocks
    private GetInfoService getInfoService;

    @Mock
    private RegisterInfoRepository registerInfoRepository;

    @Mock
    private JwtHelper jwtHelper;

    private RegisterInfoEntity entity;

    @Before
    public void setUp() {
        entity = RegisterInfoEntity.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(25000)
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .build();
    }

    @Test
    public void whenCustomerFindOwn() throws Exception {
        var request = InfoRequest.builder()
                .jwt("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJ1c2VybmFtZSI6InRlc3QwMSIsInBhc3N3b3JkIjoiYWJjMTIzIn0.YOe5AWo0HpYc4ZHuWbteqWssOpQwh3emCIskJ5YnoFA")
                .userName(null)
                .build();
        when(jwtHelper.parseBody(any())).thenReturn(JwtEntity.builder().role("customer").username("test01").password("015215").build());
        when(registerInfoRepository.findByUserNameAndIsActiveTrue(any())).thenReturn(Optional.of(entity));
        InfoResponse actual = getInfoService.execute(request);
        Assert.assertNotNull(actual);
    }

    @Test
    public void whenAdminFind() throws Exception {
        var request = InfoRequest.builder()
                .jwt("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJ1c2VybmFtZSI6InRlc3QwMSIsInBhc3N3b3JkIjoiYWJjMTIzIn0.YOe5AWo0HpYc4ZHuWbteqWssOpQwh3emCIskJ5YnoFA")
                .userName("test01")
                .build();
        when(jwtHelper.parseBody(any())).thenReturn(JwtEntity.builder().role("admin").username("admin01").password("015215").build());
        when(registerInfoRepository.findByUserNameAndIsActiveTrue(any())).thenReturn(Optional.of(entity));
        InfoResponse actual = getInfoService.execute(request);
        Assert.assertNotNull(actual);
    }

    @Test
    public void whenRegisterUserDuplicate() throws Exception {
        var request = InfoRequest.builder()
                .jwt("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJ1c2VybmFtZSI6InRlc3QwMSIsInBhc3N3b3JkIjoiYWJjMTIzIn0.YOe5AWo0HpYc4ZHuWbteqWssOpQwh3emCIskJ5YnoFA")
                .userName("test01")
                .build();
        when(jwtHelper.parseBody(any())).thenReturn(JwtEntity.builder().role("customer").username("admin02").password("015215").build());
        var exception = Assert.assertThrows(Exception.class, () -> getInfoService.execute(request));
        assertEquals("This user don't have permission to get information customer", exception.getMessage());
    }
}
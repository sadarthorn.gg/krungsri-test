package com.example.demo.helper.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.example.demo.model.entity.JwtEntity;
import com.example.demo.model.entity.RegisterInfoEntity;
import com.example.demo.model.request.RegisterRequest;
import com.example.demo.model.response.RegisterResponse;
import com.example.demo.repository.RegisterInfoRepository;
import com.example.demo.service.RegisterService;
import java.util.Optional;
import lombok.var;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class JwtHelperTest {

    @InjectMocks
    private JwtHelper jwtHelper;

    @Before
    public void setUp() {

    }

    @Test
    public void whenRegisterPlatinum() throws Exception {
        JwtEntity expected = JwtEntity.builder().role("admin").username("test01").password("abc123").build();
        JwtEntity actual = jwtHelper.parseBody("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJ1c2VybmFtZSI6InRlc3QwMSIsInBhc3N3b3JkIjoiYWJjMTIzIn0.YOe5AWo0HpYc4ZHuWbteqWssOpQwh3emCIskJ5YnoFA");
        assertThat(actual).isEqualTo(expected);
    }
}
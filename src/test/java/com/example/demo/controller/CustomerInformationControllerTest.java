package com.example.demo.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.demo.model.request.InfoRequest;
import com.example.demo.model.request.RegisterRequest;
import com.example.demo.model.response.InfoResponse;
import com.example.demo.model.response.RegisterResponse;
import com.example.demo.service.GetInfoService;
import com.example.demo.service.RegisterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class CustomerInformationControllerTest {

    @InjectMocks
    private CustomerInformationController customerInformationController;

    @Mock
    private RegisterService registerService;

    @Mock
    private GetInfoService getInfoService;

    private MockMvc mockMVC;

    @Before
    public void setUp() {
        mockMVC = MockMvcBuilders.standaloneSetup(customerInformationController).build();
    }

    @Test
    public void whenRegisterSuccess() throws Exception {
        //Given
        RegisterRequest request = RegisterRequest.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(40000)
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .build();

        RegisterResponse response = RegisterResponse.builder()
                .referenceCode("202201189100")
                .memberType("Gold")
                .build();

        //when
        when(registerService.execute(any()))
                .thenReturn(response);

        mockMVC.perform(post("/v1/register")
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andDo(print());
    }

    @Test
    public void whenGetInfoSuccess() throws Exception {
        //Given
        InfoRequest request = InfoRequest.builder()
                .userName("test01")
                .jwt("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJ1c2VybmFtZSI6InRlc3QwMSIsInBhc3N3b3JkIjoiYWJjMTIzIn0.YOe5AWo0HpYc4ZHuWbteqWssOpQwh3emCIskJ5YnoFA")
                .build();

        InfoResponse response = InfoResponse.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(40000)
                .memberType("Gold")
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .referCode("202201189100")
                .isActive(true)
                .build();

        //when
        when(getInfoService.execute(any()))
                .thenReturn(response);

        mockMVC.perform(get("/v1/info/")
                .header("Authorization",request.getJwt())
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andDo(print());
    }

    @Test
    public void whenGetInfoSuccessAdmin() throws Exception {
        //Given
        InfoRequest request = InfoRequest.builder()
                .userName("test01")
                .jwt("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJ1c2VybmFtZSI6InRlc3QwMSIsInBhc3N3b3JkIjoiYWJjMTIzIn0.YOe5AWo0HpYc4ZHuWbteqWssOpQwh3emCIskJ5YnoFA")
                .build();

        InfoResponse response = InfoResponse.builder()
                .userName("test01")
                .password("abc123")
                .firstName("test")
                .middleName(null)
                .lastName("test")
                .salary(40000)
                .memberType("Gold")
                .address1("12245/5415")
                .address2(null)
                .phoneNo("0852479100")
                .email("test@accenture.com")
                .referCode("202201189100")
                .isActive(true)
                .build();

        //when
        when(getInfoService.execute(any()))
                .thenReturn(response);

        mockMVC.perform(get("/v1/info/username")
                .header("Authorization",request.getJwt())
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andDo(print());
    }


}
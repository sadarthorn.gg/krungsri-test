### Register project

1. POST Register APIs {endpoint : /v1/register}
- This api for save register information to DB
- Curl : "curl --location --request POST 'http://localhost:8080/v1/register' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "userName": "test03",
  "password": "abc123",
  "firstName": "test",
  "middleName": null,
  "lastName": "testing",
  "salary": 52000,
  "address1": "111/254 bangkok",
  "address2": "",
  "phoneNo": "0817129100",
  "email": "jameres_brabra@hotmail.com"
  }'"
  
- If process finish without any error will return status 200 OK and response
  {
  "referenceCode": "202201249100",
  "memberType": "Platinum"
  }
  
- If user is duplicate will return error 500
  "80000 This User cannot be used"
  
- If salary < 15000 will return error 500 with code "80001" 

2. GET Information APIs {endpoint : /v1/info/{username}}
- This api is for get information per username to show
- This api have 2 mode
  - end pont /v1/info/ it's get for own user will decode data from jwt in header to find
  - end pont /v1/info/{username} it's get for admin to find another user information will decode data from jwt and check admin role in header, get username from path param to find
  
- Curl : curl --location --request GET 'http://localhost:8080/v1/info/test04' \
  --header 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJ1c2VybmFtZSI6InRlc3QwMSIsInBhc3N3b3JkIjoiYWJjMTIzIn0.YOe5AWo0HpYc4ZHuWbteqWssOpQwh3emCIskJ5YnoFA' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "userName": "test03",
  "password": "abc123",
  "firstName": "test",
  "middleName": null,
  "lastName": "testing",
  "salary": 52000,
  "address1": "111/182 khukot",
  "address2": "",
  "phoneNo": "0817129100",
  "email": "test03@hotmail.com"
  }'
  - Authorization field : 
    {
    "role": "admin",
    "username": "test01",
    "password": "abc123"
    }
    
- If process finish without any error will return status 200 OK and response
  {
  "userName": "test04",
  "password": "abc123",
  "firstName": "test",
  "middleName": null,
  "lastName": "testing",
  "salary": 52000,
  "memberType": "Platinum",
  "address1": "111/154 bangkok",
  "address2": "",
  "phoneNo": "0817129100",
  "email": "test04@hotmail.com",
  "referCode": "202201249100",
  "active": true
  }
  
- If not have path param and not have admin role will return error 500 
  Throw "This user don't have permission to get information customer"
